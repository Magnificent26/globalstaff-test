#!/bin/bash

# Проверка наличия аргумента
if [ -z "$1" ]; then
    echo "Usage: $0 seconds_to_wait"
    exit 1
fi

# Извлечение аргумента
seconds_to_wait="$1"

# Ожидание указанного количества секунд
sleep $seconds_to_wait

php /app/artisan game:classic:stop
