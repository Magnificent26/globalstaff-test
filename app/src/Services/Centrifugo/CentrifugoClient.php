<?php
namespace Services\Centrifugo;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Services\Contracts\WebsocketClient;

class CentrifugoClient implements WebsocketClient
{
    private string $apiKey;

    private string $channel = 'default';

    private array $data = [];

    public function __construct()
    {
        $this->apiKey = env('CENTRIFUGO_API_KEY');
    }

    public function setChannel(string $channel): static
    {
        $this->channel = $channel;
        return $this;
    }

    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @throws GuzzleException
     */
    public function send(): ResponseInterface
    {
        $guzzleClient = new Client();
        return $guzzleClient->post(
            $this->getCentrifugoUrl(), [
                'json' => $this->getPostData(),
                'headers' => $this->getHeaders()
            ]);
    }

    private function getCentrifugoUrl(): string
    {
        $host = env('CENTRIFUGO_HOST');
        $port = env('CENTRIFUGO_PORT');
        return sprintf('http://%s:%s/api/publish', $host, $port);
    }

    private function getPostData(): array
    {
        return [
            'channel' => $this->channel,
            'data' => $this->data
        ];
    }

    private function getHeaders(): array
    {
        return [
            "Content-Type" => "application/json",
            "X-API-Key" => $this->apiKey,
            //"Authorization" => "Bearer $this->apiKey",
        ];
    }
}
