<?php

namespace Services\Providers;

use Illuminate\Support\ServiceProvider;
use Services\Centrifugo\CentrifugoClient;
use Services\Contracts\WebsocketClient;

class ServicesServiceProvider extends ServiceProvider
{
    public array $bindings = [
        WebsocketClient::class => CentrifugoClient::class
    ];
}
