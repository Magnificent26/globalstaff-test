<?php

namespace Services\Contracts;

use Psr\Http\Message\ResponseInterface;

interface WebsocketClient
{
    public function send(): ResponseInterface;

    public function setData(array $data): static;

    public function setChannel(string $channel): static;
}
