<?php

namespace Domain\Auth\DTOs;

use Illuminate\Foundation\Http\FormRequest;

class AuthLoginDTO extends DTO
{
    private string $email;

    private string $password;

    protected function setData(FormRequest $request): void
    {
        $validated = $request->validated();
        $this->email = $validated['email'];
        $this->password = $validated['password'];
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function toArray(): array
    {
        return [
            'email' => $this->getEmail(),
            'password' => $this->getPassword()
        ];
    }
}
