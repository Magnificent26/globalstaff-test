<?php

namespace Domain\Auth\Contracts;

use Domain\Auth\DTOs\AuthLoginDTO;

interface LogInUserContracts
{
    public function __invoke(AuthLoginDTO $dto): string;
}
