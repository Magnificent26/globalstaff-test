<?php

namespace Domain\Auth\Actions;

use Domain\Auth\Contracts\LogInUserContracts;
use Domain\Auth\DTOs\AuthLoginDTO;
use Domain\Auth\Exceptions\LoginException;

class LoginUserAction implements LogInUserContracts
{
    /**
     * @throws LoginException
     */
    public function __invoke(AuthLoginDTO $dto): string
    {
        if (!$token = auth('api')->attempt($dto->toArray())) {
            throw new LoginException('Invalid password', 403);
        }



        return $token;
    }
}
