<?php

namespace Domain\Auth\Providers;

use Domain\Auth\Actions\LoginUserAction;
use Domain\Auth\Contracts\LogInUserContracts;
use Illuminate\Support\ServiceProvider;

class ActionsServiceProvider extends ServiceProvider
{
    public array $bindings = [
        LogInUserContracts::class => LoginUserAction::class
    ];
}
