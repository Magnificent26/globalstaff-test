<?php

namespace Domain\GameClassic\Actions;

use Domain\GameClassic\Contracts\GameBetContracts;
use Domain\GameClassic\DTOs\BetDTO;
use Domain\GameClassic\Event\GameClassicBetCreated;
use Domain\GameClassic\Exceptions\AvailableGameNotFoundException;
use Domain\GameClassic\Exceptions\BetException;
use Domain\GameClassic\Models\GameClassic;
use Domain\GameClassic\Models\GameClassicBet;
use Domain\GameClassic\Processes\GameBetProcess;
use Domain\GameClassic\Processes\WriteOffMoneyFromUser;
use Support\Transaction;
use Throwable;

class BetAction implements GameBetContracts
{
    /**
     * @throws AvailableGameNotFoundException
     * @throws BetException|Throwable
     */
    public function __invoke(BetDTO $dto): void
    {
        Transaction::run(function() use ($dto) {
            /* @var GameClassic $game */
            $game = GameClassic::query()->whereNull('finish_time')->lockForUpdate()->first();
            if (!$game) {
                throw new AvailableGameNotFoundException(__('game.errors.available_game_not_found'), 400);
            }

            /* @var GameClassicBet $bet */
            $bet = $game->bets()->create([
                'amount' => $dto->getAmount(),
                'user_id' => $dto->getUserId(),
            ]);

            return (new GameBetProcess($bet))->processes([
                new WriteOffMoneyFromUser(),
            ])->run();
        }, function (GameClassicBet $bet) {
            event(new GameClassicBetCreated($bet));
        }, function (Throwable $exception) {
            throw new BetException($exception->getMessage(), 400);
        });

    }
}
