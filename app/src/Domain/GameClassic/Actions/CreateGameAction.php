<?php

namespace Domain\GameClassic\Actions;

use Domain\GameClassic\Contracts\GameCreateContracts;
use Domain\GameClassic\Event\GameCreated;
use Domain\GameClassic\Models\GameClassic;

class CreateGameAction implements GameCreateContracts
{
    public function __invoke(): GameClassic
    {
        /* @var ?GameClassic $gamesQuantity */
        $gamesQuantity = GameClassic::query()->whereNull('finish_time')->first();
        if ($gamesQuantity) {
            return $gamesQuantity;
        }

        /* @var GameClassic $game */
        $game = GameClassic::query()->create();
        event(new GameCreated($game));
        return $game;
    }
}
