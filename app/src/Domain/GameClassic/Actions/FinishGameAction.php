<?php

namespace Domain\GameClassic\Actions;

use Domain\GameClassic\Contracts\GameFinishContracts;
use Domain\GameClassic\Event\GameFinished;
use Domain\GameClassic\Exceptions\GameFinishException;
use Domain\GameClassic\Models\GameClassic;
use Support\Transaction;
use Throwable;

class FinishGameAction implements GameFinishContracts
{
    /**
     * @throws GameFinishException
     * @throws Throwable
     */
    public function __invoke(GameClassic $game): void
    {
        Transaction::run( function () use ($game) {
            $game->fill([
                'finish_time' => now()
            ])->save();
            event(new GameFinished($game));
        }, onError: function (Throwable $exception) {
            throw new GameFinishException($exception->getMessage(), 400);
        });
    }
}
