<?php

namespace Domain\GameClassic\Actions;

use Domain\GameClassic\Contracts\GameStartContracts;
use Domain\GameClassic\Event\GameStarted;
use Domain\GameClassic\Models\GameClassic;

class StartGameAction implements GameStartContracts
{
    public function __invoke(GameClassic $game): void
    {
        if ($game->start_time !== null) {
            return;
        }

        $game->fill([
            'start_time' => now()
        ])->save();
        event(new GameStarted($game));
    }
}
