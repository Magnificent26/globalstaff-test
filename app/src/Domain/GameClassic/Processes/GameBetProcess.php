<?php

namespace Domain\GameClassic\Processes;

use Domain\GameClassic\Models\GameClassicBet;
use Illuminate\Pipeline\Pipeline;

class GameBetProcess
{
    protected array $processes = [];

    public function __construct(
        protected GameClassicBet $bet
    ) {
    }

    public function processes(array $processes): self
    {
        $this->processes = $processes;
        return $this;
    }

    public function run(): GameClassicBet
    {
        app(Pipeline::class)
            ->send($this->bet)
            ->through($this->processes)
            ->thenReturn();

        return $this->bet;
    }
}
