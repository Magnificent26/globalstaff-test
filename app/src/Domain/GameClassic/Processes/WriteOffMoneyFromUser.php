<?php

namespace Domain\GameClassic\Processes;

use Domain\Auth\Models\User;
use Domain\GameClassic\Contracts\GameBetProcessContracts;
use Domain\GameClassic\Exceptions\InsufficientFundsException;
use Domain\GameClassic\Models\GameClassicBet;

class WriteOffMoneyFromUser implements GameBetProcessContracts
{
    /**
     * @throws InsufficientFundsException
     */
    public function handle(GameClassicBet $bet, $next)
    {
        /* @var User $user */
        $user = $bet->user()->lockForUpdate()->first();
        if ($user->balance < $bet->amount) {
            throw new InsufficientFundsException(__('game.errors.insufficient_funds'), 400);
        }
        $user->decrement('balance', $bet->amount);
    }
}
