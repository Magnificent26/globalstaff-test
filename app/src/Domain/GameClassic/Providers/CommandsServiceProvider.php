<?php

namespace Domain\GameClassic\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;

class CommandsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerCommands();
    }

    private function registerCommands()
    {
        $commandPath = base_path('src/Domain/GameClassic/Commands');

        if (File::isDirectory($commandPath)) {
            $commands = File::files($commandPath);

            foreach ($commands as $command) {
                $this->commands('Domain\\GameClassic\\Commands\\' . $command->getBasename('.php'));
            }
        }
    }

    public static function schedule(Schedule $schedule)
    {
        $schedule->command('game:classic:create')->everyMinute();;
    }
}
