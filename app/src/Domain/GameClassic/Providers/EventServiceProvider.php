<?php

namespace Domain\GameClassic\Providers;

use Domain\GameClassic\Event\GameClassicBetCreated;
use Domain\GameClassic\Event\GameCreated;
use Domain\GameClassic\Event\GameFinished;
use Domain\GameClassic\Event\GameStarted;
use Domain\GameClassic\Listeners\ChargeMoneyToUser;
use Domain\GameClassic\Listeners\CreateNewGame;
use Domain\GameClassic\Listeners\DetermineWinner;
use Domain\GameClassic\Listeners\EnableStopGameTimer;
use Domain\GameClassic\Listeners\BetCreatedNotifyWebsocket;
use Domain\GameClassic\Listeners\GameCreatedNotifyWebsocket;
use Domain\GameClassic\Listeners\GameResultNotifyWebsocket;
use Domain\GameClassic\Listeners\GameStartedNotifyWebsocket;
use Domain\GameClassic\Listeners\TryStartGame;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        GameClassicBetCreated::class => [
            TryStartGame::class,
            BetCreatedNotifyWebsocket::class,
        ],
        GameCreated::class => [
            GameCreatedNotifyWebsocket::class
        ],
        GameStarted::class => [
            EnableStopGameTimer::class,
            GameStartedNotifyWebsocket::class,
        ],
        GameFinished::class => [
            DetermineWinner::class,
            ChargeMoneyToUser::class,
            GameResultNotifyWebsocket::class,
            CreateNewGame::class
        ]
    ];
}
