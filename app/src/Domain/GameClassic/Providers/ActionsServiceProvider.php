<?php

namespace Domain\GameClassic\Providers;

use Domain\GameClassic\Actions\BetAction;
use Domain\GameClassic\Actions\CreateGameAction;
use Domain\GameClassic\Actions\FinishGameAction;
use Domain\GameClassic\Actions\StartGameAction;
use Domain\GameClassic\Contracts\GameBetContracts;
use Domain\GameClassic\Contracts\GameCreateContracts;
use Domain\GameClassic\Contracts\GameFinishContracts;
use Domain\GameClassic\Contracts\GameStartContracts;
use Illuminate\Support\ServiceProvider;

class ActionsServiceProvider extends ServiceProvider
{
    public array $bindings = [
        GameBetContracts::class => BetAction::class,
        GameCreateContracts::class => CreateGameAction::class,
        GameStartContracts::class => StartGameAction::class,
        GameFinishContracts::class => FinishGameAction::class,
    ];
}
