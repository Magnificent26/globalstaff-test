<?php

namespace Domain\GameClassic\Providers;

use Illuminate\Support\ServiceProvider;

class GameClassicServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->register(
            ActionsServiceProvider::class
        );

        $this->app->register(
            EventServiceProvider::class
        );

        $this->app->register(
            CommandsServiceProvider::class
        );
    }
}
