<?php

namespace Domain\GameClassic\DTOs;

use Illuminate\Foundation\Http\FormRequest;

class BetDTO extends DTO
{
    private int $amount;

    private array $user;

    protected function setData(FormRequest $request): void
    {
        $validated = $request->validated();
        $this->amount = $validated['amount'];
        $this->setUser();
    }

    private function setUser(): void
    {
        $this->user = [
            'id' => auth('api')->id()
        ];
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getUserId(): int
    {
        return $this->user['id'];
    }

    public function toArray(): array
    {
        return [
            'amount' => $this->getAmount(),
            'user' => $this->user
        ];
    }
}
