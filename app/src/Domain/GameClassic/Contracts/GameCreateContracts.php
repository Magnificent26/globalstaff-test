<?php

namespace Domain\GameClassic\Contracts;

use Domain\GameClassic\Models\GameClassic;

interface GameCreateContracts
{
    public function __invoke(): GameClassic;
}
