<?php

namespace Domain\GameClassic\Contracts;

use Domain\GameClassic\Models\GameClassicBet;

interface GameBetProcessContracts
{
    public function handle(GameClassicBet $bet, $next);
}
