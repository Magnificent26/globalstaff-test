<?php

namespace Domain\GameClassic\Contracts;

use Domain\GameClassic\DTOs\BetDTO;

interface GameBetContracts
{
    public function __invoke(BetDTO $dto): void;
}
