<?php

namespace Domain\GameClassic\Contracts;

use Domain\GameClassic\Models\GameClassic;

interface GameFinishContracts
{
    public function __invoke(GameClassic $game): void;
}
