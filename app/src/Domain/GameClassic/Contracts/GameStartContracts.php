<?php

namespace Domain\GameClassic\Contracts;

use Domain\GameClassic\Models\GameClassic;

interface GameStartContracts
{
    public function __invoke(GameClassic $game): void;
}
