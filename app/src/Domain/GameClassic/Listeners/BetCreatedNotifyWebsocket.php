<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Contracts\GameStartContracts;
use Domain\GameClassic\Event\GameClassicBetCreated;
use Domain\GameClassic\Models\GameClassic;
use Services\Contracts\WebsocketClient;

class BetCreatedNotifyWebsocket
{
    public function __construct(
        protected WebsocketClient $websocketClient
    ) {

    }

    public function handle(GameClassicBetCreated $event): void
    {
        $this->websocketClient->setChannel('classic-game');
        $this->websocketClient->setData([
            'event' => 'bet_created',
            'game_id' => $event->bet->game->id,
            'bet_id' => $event->bet->id,
            'user_id' => $event->bet->user_id,
            'user_name' => $event->bet->user->name,
            'amount' => $event->bet->amount
        ]);
        $this->websocketClient->send();
    }
}
