<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Event\GameFinished;
use Domain\GameClassic\Models\GameClassic;

class DetermineWinner
{
    public function handle(GameFinished $event): void
    {
        if ($this->isSystemWon()) {
            $this->wonSystem($event->game);
            return;
        }

        $userId = $this->determineWonUser($event->game);
        $this->recordResult($userId, $event->game);
    }

    protected function determineWonUser(GameClassic $game): int
    {
        $wonNumber = mt_rand(0, $game->getAmount());
        $id = null;
        $checkedAmount = 0;
        foreach ($game->bets as $bet) {
            if ($id) {
                continue;
            }
            $checkedAmount += $bet->amount;
            if ($checkedAmount >= $wonNumber) {
                $id = $bet->user_id;
            }
        }
        
        return $id;
    }

    protected function wonSystem(GameClassic $game): void
    {
        $this->recordResult('system', $game);
    }

    protected function isSystemWon(): bool
    {
        return mt_rand(1, 100) <= GameClassic::getSystemWinProbability();
    }

    private function recordResult(int|string $winner, GameClassic $game): void
    {
        $game->fill([
            'result' => [
                'winner' => $winner,
                'amount' => $game->getAmount(),
            ]
        ])->save();
    }
}
