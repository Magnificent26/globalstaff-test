<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Event\GameStarted;
use Services\Contracts\WebsocketClient;

class GameStartedNotifyWebsocket
{
    public function __construct(
        protected WebsocketClient $websocketClient
    ) {
    }

    public function handle(GameStarted $event): void
    {
        $this->websocketClient->setChannel('classic-game');
        $this->websocketClient->setData([
            'event' => 'game_started',
            'game_id' => $event->game->id
        ]);
        $this->websocketClient->send();
    }
}
