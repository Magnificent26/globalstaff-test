<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Contracts\GameCreateContracts;
use Domain\GameClassic\Event\GameFinished;

class CreateNewGame
{
    public function __construct(
        protected GameCreateContracts $action
    ) {
    }

    public function handle(GameFinished $event): void
    {
        $action = $this->action;
        $action();
    }
}
