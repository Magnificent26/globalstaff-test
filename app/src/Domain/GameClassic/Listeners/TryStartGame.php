<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Contracts\GameStartContracts;
use Domain\GameClassic\Event\GameClassicBetCreated;
use Domain\GameClassic\Models\GameClassic;

class TryStartGame
{
    public function __construct(
        protected GameStartContracts $gameStart
    ) {

    }

    public function handle(GameClassicBetCreated $event): void
    {
        $game = $event->bet->game;
        $quantity = $game->getUniqueMembersQuantity();

        if ($quantity >= GameClassic::MINIMUM_PARTICIPANTS) {
            $action = $this->gameStart;
            $action($game);
        }
    }
}
