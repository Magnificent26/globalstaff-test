<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Event\GameStarted;
use Domain\GameClassic\Models\GameClassic;

class EnableStopGameTimer
{
    private const BASH_SCRIPT_PATH = '/app/bash/stop_classic_game.sh';

    public function handle(GameStarted $event): void
    {
        $path = static::BASH_SCRIPT_PATH;
        $seconds = GameClassic::getGameDuration();
        exec("bash $path $seconds > /dev/null 2>&1 &");
    }
}
