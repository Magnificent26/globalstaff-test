<?php

namespace Domain\GameClassic\Listeners;

use Domain\Auth\Models\User;
use Domain\GameClassic\Event\GameFinished;

class ChargeMoneyToUser
{
    public function handle(GameFinished $event): void
    {
        $winnerId = $event->game->result['winner'];
        if ($winnerId === 'system') {
            return;
        }

        User::query()
            ->lockForUpdate()
            ->where('id', $winnerId)
            ->increment('balance', $event->game->result['amount']);
    }
}
