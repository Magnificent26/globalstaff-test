<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Event\GameFinished;
use Services\Contracts\WebsocketClient;

class GameResultNotifyWebsocket
{
    public function __construct(
        protected WebsocketClient $websocketClient
    ) {

    }

    public function handle(GameFinished $event): void
    {
        $this->websocketClient->setChannel('classic-game');
        $this->websocketClient->setData([
            'event' => 'game_finished',
            'game_id' => $event->game->id,
            'amount' => $event->game->result['amount'],
            'winner' => $event->game->result['winner'],
        ]);
        $this->websocketClient->send();
    }
}
