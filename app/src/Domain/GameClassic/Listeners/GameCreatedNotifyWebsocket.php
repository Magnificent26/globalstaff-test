<?php

namespace Domain\GameClassic\Listeners;

use Domain\GameClassic\Event\GameCreated;
use Domain\GameClassic\Models\GameClassic;
use Services\Contracts\WebsocketClient;

class GameCreatedNotifyWebsocket
{
    public function __construct(
        protected WebsocketClient $websocketClient
    ) {
    }

    public function handle(GameCreated $event): void
    {
        $this->websocketClient->setChannel('classic-game');
        $this->websocketClient->setData([
            'event' => 'game_created',
            'game_id' => $event->game->id,
            'timer' => GameClassic::getGameDuration()
        ]);
        $this->websocketClient->send();
    }
}
