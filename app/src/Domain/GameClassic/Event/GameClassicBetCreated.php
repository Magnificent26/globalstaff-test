<?php

namespace Domain\GameClassic\Event;

use Domain\GameClassic\Models\GameClassicBet;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GameClassicBetCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(
        public GameClassicBet $bet
    ) {

    }
}
