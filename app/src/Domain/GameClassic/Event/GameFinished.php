<?php

namespace Domain\GameClassic\Event;

use Domain\GameClassic\Models\GameClassic;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GameFinished
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(
        public GameClassic $game
    ) {
    }
}
