<?php

namespace Domain\GameClassic\Models;

use Domain\GameClassic\Event\GameFinished;
use Domain\GameClassic\Event\GameStarted;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * @property int id
 * @property ?array result
 * @property null|string|Carbon start_time
 * @property null|string|Carbon finish_time
 * @property null|string|Carbon created_at
 * @property null|string|Carbon updated_at
 * @property null|string|Carbon deleted_at
 * @property Collection|GameClassicBet[] bets
 */
class GameClassic extends Model
{
    use SoftDeletes;

    const MINIMUM_PARTICIPANTS = 2;
    private const GAME_DURATION = 30;
    private const SYSTEM_WIN_PROBABILITY = 5;

    protected $fillable = [
        'result',
        'start_time',
        'finish_time'
    ];

    protected $casts = [
        'result' => 'json',
        'start_time' => 'datetime',
        'finish_time' => 'datetime'
    ];

    public function bets(): HasMany
    {
        return $this->hasMany(GameClassicBet::class, 'game_classic_id');
    }

    public function getUniqueMembersQuantity(): int
    {
        $members = [];
        foreach ($this->bets as $bet) {
            $members[] = $bet->user_id;
        }
        return count(array_unique($members));
    }

    public static function getGameDuration(): int
    {
        return static::GAME_DURATION;
    }

    public static function getSystemWinProbability(): int
    {
        return static::SYSTEM_WIN_PROBABILITY;
    }

    public function getAmount(): int
    {
        $amount = 0;
        foreach ($this->bets as $bet) {
            $amount += $bet->amount;
        }
        return $amount;
    }
}
