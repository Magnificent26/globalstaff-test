<?php

namespace Domain\GameClassic\Models;


use Domain\Auth\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * @property int id
 * @property int amount
 * @property int user_id
 * @property int game_classic_id
 * @property null|string|Carbon created_at
 * @property null|string|Carbon updated_at
 * @property null|string|Carbon deleted_at
 * @property null|User user
 * @property GameClassic game
 */
class GameClassicBet extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'amount',
        'user_id',
        'game_classic_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function game(): BelongsTo
    {
        return $this->belongsTo(GameClassic::class, 'game_classic_id', 'id');
    }
}
