<?php

namespace Domain\GameClassic\Commands;

use Domain\GameClassic\Contracts\GameFinishContracts;
use Domain\GameClassic\Exceptions\AvailableGameNotFoundException;
use Domain\GameClassic\Models\GameClassic;
use Illuminate\Console\Command;

class FinishGame extends Command
{

    public function __construct(
        protected GameFinishContracts $gameFinishAction
    ) {
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:classic:stop';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Stop a classic game';

    /**
     * Execute the console command.
     * @throws AvailableGameNotFoundException
     */
    public function handle()
    {
        /* @var GameClassic $game */
        $game = GameClassic::query()->whereNull('finish_time')->first();
        if (!$game) {
            throw new AvailableGameNotFoundException(__('game.errors.available_game_not_found'), 400);
        }
        $action = $this->gameFinishAction;
        $action($game);
    }
}
