<?php

namespace Domain\GameClassic\Commands;

use Domain\GameClassic\Contracts\GameCreateContracts;
use Domain\GameClassic\Models\GameClassic;
use Illuminate\Console\Command;

class CreateGame extends Command
{
    public function __construct(
        protected GameCreateContracts $gameCreateAction
    ) {
        parent::__construct();
    }

    protected $signature = 'game:classic:create';

    protected $description = 'Create a classic game';


    public function handle()
    {


        $action = $this->gameCreateAction;
        $action();
    }
}
