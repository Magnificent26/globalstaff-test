<?php
namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;

abstract class DTO
{
    public function __construct(FormRequest $request)
    {
        $this->setData($request);
    }

    abstract protected function setData(FormRequest $request): void;

    abstract public function toArray(): array;
}
