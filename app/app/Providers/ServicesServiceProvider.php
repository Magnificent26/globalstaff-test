<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Services\Providers\ServicesServiceProvider as Services;

class ServicesServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        $this->app->register(
            Services::class
        );
    }

}
