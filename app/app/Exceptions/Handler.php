<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (Throwable $e, Request $request) {
            if (app()->environment('production')) {
                $code = $e->getCode() !== 0 ? $e->getCode() : 403;
                if ($request->is('api/*')) {
                    return response()->json([
                        'message' => $e->getMessage(),
                    ], $code);
                }
            }
        });

        $this->reportable(function (Throwable $e) {

        });
    }
}
