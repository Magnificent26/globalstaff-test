<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Faker\Generator as Faker;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create sample user';

    /**
     * Execute the console command.
     */
    public function handle(Faker $faker)
    {
        $name = $faker->name;
        $email = $faker->email;
        $password = $faker->password;
        $balance = mt_rand(10000, 100000);
        $responseMessage = $this->generateResponseMessage($email, $password, $balance);
        $password = bcrypt($password);
        $data = compact('name', 'email', 'password', 'balance');
        User::query()->create($data);
        echo $responseMessage;
    }

    private function generateResponseMessage(string $email, string $password, int $balance): string
    {
        return "Created user:\n- email: $email\n- password: $password\n- balance: $balance\n";
    }
}
