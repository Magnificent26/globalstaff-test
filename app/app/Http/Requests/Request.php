<?php

namespace App\Http\Requests;

use App\DTOs\DTO;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [];
    }

    abstract public function getDto(): DTO;


    abstract protected function errorMessages(): array;

    public function messages(): array
    {
        return array_merge(parent::messages(), $this->errorMessages());
    }
}
