<?php

namespace App\Http\Requests\User\Auth;

use App\DTOs\DTO;
use App\Http\Requests\Request;
use Domain\Auth\DTOs\AuthLoginDTO;
use Illuminate\Validation\Rule;

class LoginRequest extends Request
{
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email:dns',
                Rule::exists('users', 'email')->whereNull('deleted_at')
            ],
            'password' => 'required'
        ];
    }

    public function getDto(): DTO
    {
        return new AuthLoginDTO($this);
    }

    protected function errorMessages(): array
    {
        return [
            'email.required' => __('auth.email_required'),
            'email.email' => __('auth.email_invalid'),
            'email.exists' => __('auth.email_not_exists'),
            'password.required' => __('auth.password_required'),
        ];
    }
}
