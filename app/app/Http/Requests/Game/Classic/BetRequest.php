<?php

namespace App\Http\Requests\Game\Classic;

use App\DTOs\DTO;
use App\Http\Requests\Request;
use Domain\GameClassic\DTOs\BetDTO;
use Illuminate\Foundation\Http\FormRequest;

class BetRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'amount' => 'required|numeric'
        ];
    }

    public function getDto(): DTO
    {
        return new BetDTO($this);
    }

    protected function errorMessages(): array
    {
        return [
            'amount.required' => '',
            'amount.numeric' => ''
        ];
    }
}
