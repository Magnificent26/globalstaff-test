<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\LoginRequest;
use Domain\Auth\Contracts\LogInUserContracts;
use Domain\Auth\DTOs\AuthLoginDTO;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class AuthController extends Controller
{
    public function login(LogInUserContracts $loginUser, LoginRequest $request): JsonResponse
    {
        /* @var AuthLoginDTO $dto */
        $dto = $request->getDto();
        $token = $loginUser($dto);


        return response()->json([
            'access_token' => $token,
            'type' => 'Bearer',
            'expires_in' => Config::get('jwt.ttl') * 60
        ]);
    }
}
