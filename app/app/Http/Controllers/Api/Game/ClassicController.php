<?php

namespace App\Http\Controllers\Api\Game;

use App\Http\Controllers\Controller;
use App\Http\Requests\Game\Classic\BetRequest;
use Domain\GameClassic\Contracts\GameBetContracts;
use Domain\GameClassic\DTOs\BetDTO;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ClassicController extends Controller
{
    public function bet(GameBetContracts $gameBet, BetRequest $request): JsonResponse
    {
        /* @var BetDTO $dto */
        $dto = $request->getDto();
        $gameBet($dto);

        return response()->json([
            'status' => true
        ]);
    }
}
