<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\Game\ClassicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('auth')
    ->middleware('api')
    ->controller(AuthController::class)
    ->group(function () {
        Route::post('login', 'login');
    });

Route::prefix('game')->group(function () {
    Route::prefix('classic')
        ->middleware(['api', 'auth:api'])
        ->controller(ClassicController::class)
        ->group(function () {
            Route::post('bet', 'bet');
        });
});


